;;;; wavebuild-combine.lisp - combining elements demo
(defpackage :vxsh-suite.wavebuild.combine
	(:shadowing-import-from :vxsh-suite.wavebuild.hyle
		#:element-name-to-symbol
		#:hyle  #:hyle-p #:make-hyle #:hyle-fallback-info
		#:hyle-id #:hyle-icon #:hyle-name #:hyle-width #:hyle-moves #:hyle-sum #:hyle-subsetting #:hyle-expressing
		#:hyle< #:hyle-moves-+ #:hyle-sum-+ #:hyle-subset-+  #:hyle-query)
	(:use :common-lisp)
	(:export
		#:*hyle-width* #:*hyle-table* #:*hyle-list*  ; #::init-hyle-table
		#:hyle-string #:hyle-list
		#:set-hyle #:set-hyle*  #:get-hyle #:list-to-hyle #:hyle-to-list
		#:get-characteristic #:set-characteristic #:get-combination
		#:hyle-mix #:hyle-mix*
		#:color-terminal-icon))
	;; friendly counterparts exported in :wavebuild
		;; #:all #:tall
		;; #:put #:tput  #:net #:tnet
		;; #:mix #:tmix
(in-package :vxsh-suite.wavebuild.combine)


;;; HYLE storage and listing

(defvar *hyle-width*)  ; length of element :name assuming monospace characters
(defvar *hyle-table*)  ; table storing all elements
(defvar *hyle-list*)   ; flat 'list of all elements by internal :id

(defun init-hyle-table ()  ; initialize element metadata
	(setq
		*hyle-table*
			(if (and (boundp '*hyle-table*) (not (null *hyle-table*)))
				*hyle-table*  (make-hash-table :test 'equal))
		*hyle-list*
			(if (and (boundp '*hyle-list*)  (not (null *hyle-list*)))
				*hyle-list*   (list))
        *hyle-width*
            (if (and (boundp '*hyle-width*) (> *hyle-width* 1))
                *hyle-width*   25)))

(defun set-hyle (&key name id icon descriptor moves sum subsetting expressing text)
;; :wavebuild interface - see 'SET-HYLE*
    (init-hyle-table)
    (let (known-hyle hyle icon name name-width)
    (setf
        ;; attempt to get existing element
        id  (hyle-fallback-info descriptor 'hyle-id id nil)
        known-hyle  (or  (get-hyle id)  (make-hyle))
        ;; create element to be stored
        hyle
            (make-hyle
                :descriptor descriptor  :existing known-hyle
                :name name  :id id  :icon icon
                :moves moves  :sum sum  :subsetting subsetting  :expressing expressing
                )
        name  (hyle-name hyle)
        name-width  (hyle-width hyle)
        *hyle-width*
            (if (> name-width *hyle-width*)  name-width  *hyle-width*)
        *hyle-list*
            (remove-duplicates (cons id *hyle-list*))
        (gethash id *hyle-table*)  hyle
        (gethash name *hyle-table*)
            (if (null name)  nil  hyle)
        )
    (values
        ;; return stored HYLE
        (if (null text)  hyle  (hyle-string hyle))
        ;; return extra debug information - these should not be taken as a stable API
        *hyle-list*  *hyle-width*)))

(defun set-hyle* (descriptor  &key name id icon moves sum subsetting expressing text)
;; :wavebuild interface - may be called as 'PUT / 'TPUT  -  (put DESCRIPTOR ...) (tput DESCRIPTOR ...)
    (set-hyle :descriptor descriptor
        :name name :id id :icon icon :moves moves :sum sum :subsetting subsetting :expressing expressing :text text))

(defun get-hyle (descriptor)
;; :wavebuild interface - may be called as 'NET / 'TNET  -  (net DESCRIPTOR) (tnet DESCRIPTOR)
;; for 'TNET see 'HYLE-STRING
    (init-hyle-table)
    (let ((hyle
            (or  (gethash descriptor *hyle-table*)
                 (gethash (hyle-id (make-hyle :descriptor descriptor)) *hyle-table*)
                 )))
        hyle))

(defun (setf get-hyle) (value element)
    (set-hyle :descriptor element))

(defun list-to-hyle (hyle-list)
    (let (result)
    (dolist (hyle hyle-list)
        (setq result
            (cons (get-hyle hyle) result)))
    (reverse result)))

(defun hyle-to-list (hyle-list)
    (let (result)
    (dolist (hyle hyle-list)
        (setq result
            (cons (hyle-id hyle) result)))
    ;; for very unclear reasons 'reverse caused a bug? yet not with 'list-to-hyle
    (nreverse result)))

(defun hyle-stable-sort (hyle-list)
    (hyle-to-list
        (stable-sort
            (list-to-hyle hyle-list)
            #'hyle<)))

(defun get-characteristic (a b)        ; fetch value for RDF-style property
    ;; NOTE: you currently must set-characteristic *exactly* the element ID you want to pull from. these do not do fuzzy lookups
    (multiple-value-bind (query sorted) (hyle-query a b)
    (let* ((lookup (gethash query *hyle-table*)))
    (values lookup query sorted))))

(defun set-characteristic (a b value)  ; store value for RDF-style property
    (multiple-value-bind (query sorted) (hyle-query a b)
    (setf (gethash query *hyle-table*)  value)
    (values value sorted)))

(defun get-combination (a b)           ; fetch HYLE structure for combination result
    (get-characteristic a b))

(defun (setf get-combination) (result hyle-b hyle-a)
    (set-characteristic hyle-a hyle-b result)
    (set-hyle :descriptor result))

(defun color-terminal-icon (icon-string)  ; BUG: this doesn't work yet. but it will one day?
    (format *standard-output* "~A" "#\Escape[1;32m🧩︎#\Escape[0m"))

;; BUG: unknown elements displayed as NIL
(defun hyle-subsetting-string (subsetting)
    (let ((output (make-string-output-stream)) icon)
    (dolist (element subsetting)
        ;; this function has to be defined after 'get-hyle to get the icons
        (setq
            element (get-hyle element)
            icon    (hyle-icon element)
            icon    (if (null icon)  "🧩"  icon))
        (format output "~A~a ~a"
            " 　"  ; '　' = ideographic space
            icon  (hyle-name element)))
    (get-output-stream-string output)))

(defun hyle-definition (element)
    (multiple-value-bind
        (lookup query sorted) (get-characteristic element 'vxsh-suite.wavebuild.q::hyle-definition)
        lookup))

(defun hyle-string (element &key subsetting)  ; string for printing one or more elements to :text
    (init-hyle-table)
    (setq element (make-hyle :descriptor element))
    (let ((max-width (+ *hyle-width* 2))
          (icon (hyle-icon element)) (name (hyle-name element)) (id (hyle-id element)))
    (print (list "TROUBLE ID" id))
    (setf
        icon  (if (null icon)  "🧩"      icon)
        name  (if (null name)  "[None]"  name))
    (format nil
        (format nil "~~~aA  ~~a:~~a  ~~a  ~~a~~%~A"  ; e.g. "~10A  ~a:~a ..."
            max-width
            (if (null subsetting)  ""  "~%~A~%~%~a~A~%~%~a~A~%"
                ))
        name   (hyle-sum element) (hyle-moves element) (hyle-id id :text t) icon  ; (hyle-id element :text t)
        ;; key :subsetting refers to whether to *print* :subsetting
        (or  (hyle-definition id)  "(no definition)")
            ;; BUG: definitions aren't tested with prefixes and might be broken
        ":SUBSETTING"  (hyle-subsetting-string (hyle-subsetting element))
        ":EXPRESSING"  (hyle-subsetting-string (hyle-expressing element))
        )
    ))

(defun hyle-list (&key text)
;; :wavebuild interface - may be called as 'ALL / 'TALL  -  (all) (tall)
    (init-hyle-table)
    (cond
        ((null text) *hyle-list*)
        (t
            (let ((output (make-string-output-stream))
                  ;; print elements from first to last so the most recent ones are easy to find on a terminal
                  (elements (reverse *hyle-list*)))
            (format output "~%")
            (dolist (hyle elements)
                (format output "~a~%" (hyle-string (get-hyle hyle))))
            (get-output-stream-string output)))))

;; LATER: put the shorthand 'tall versions of elements in table so it's simple to dereference them
(defun hyle-mix (hyle-a hyle-b  &key result text icon moves sum)  ; combine two elements
;; :wavebuild interface - see 'HYLE-MIX*
    (init-hyle-table)
    (let (hyle1 hyle2 lookup produced result-sum id)

    ;; look up combination, always in alphabetical order by internal :id
    (multiple-value-bind (lookup query sorted) (get-combination hyle-a hyle-b)
        (setf
            hyle1   (first  sorted)
            hyle2   (second sorted)
            produced
                (cond
                    ((not (null result))  (make-hyle :descriptor result))
                    ((not (null lookup))  lookup)
                    (t nil)
                    ))
        ;; if anything was produced, add element and combination to table
        ;; remember HYLE structure -  #(HYLE :id :icon :name :moves :sum :subsetting :expressing)
        (unless (null produced)
            (setf
                ;; use resulting icon unless passed :icon
                (hyle-icon produced)
                    (if (null icon)  (hyle-icon produced)  icon)

                moves  (if (null moves)  (hyle-moves result)  moves)  ; use if passed :moves
                sum    (if (null sum)    (hyle-sum result)    sum)    ; use if passed :sum

                (hyle-moves produced)
                    (cond
                        ((and (integerp moves) (> moves 0))  moves)
                        ((and
                              (not (null result))
                              (not (null (hyle-moves hyle-a))) (not (null (hyle-moves hyle-b))))
                            (hyle-moves-+ hyle-a hyle-b))
                        (t 0))

                (hyle-sum produced)
                    (if (and (integerp sum) (> sum 0))
                        sum
                        (hyle-sum-+ hyle-a hyle-b))

                (hyle-subsetting produced)
                    (hyle-stable-sort
                        (hyle-subset-+
                            (list produced) (hyle-subsetting hyle1) (hyle-subsetting hyle2)))
                
                (hyle-expressing produced)
                    (hyle-stable-sort
                        (remove-duplicates
                            (hyle-subset-+
                                (list produced) (hyle-expressing hyle1) (hyle-expressing hyle2))))

                ;; update element in table and store combination
                (get-combination hyle1 hyle2)  produced
                ))
        (if (null text)             ; only if terminal-friendly output requested...
            (values produced lookup query result)  ; normally, return debug information
            (hyle-string produced))  ; ...return just an element string
)))

(defun hyle-mix* (hyle-a hyle-b  &key result text icon moves sum)  ; fuzzy version of HYLE-MIX
;; :wavebuild interface - may be called as 'MIX / 'TMIX  -  (mix HYLE-A HYLE-B) (tmix HYLE-A HYLE-B RESULT)
    (let (hyle1 hyle2)
    (setq
        ;; get element by descriptor,
        ;; or fill in with null elements if they are missing
        hyle1  (or (get-hyle hyle-a) (make-hyle :descriptor hyle-a))
        hyle2  (or (get-hyle hyle-b) (make-hyle :descriptor hyle-b))
        )
    (hyle-mix hyle1 hyle2 :result result :text text :icon icon :moves moves :sum sum)
    ))
