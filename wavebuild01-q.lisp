;;;; wavebuild-q.lisp - namespace for storing elements
;; inline comments represent how terms are defined as elements in system.dict

(defpackage :vxsh-suite.wavebuild.hyle-struct  ; model of HYLE structure - see hyle.lisp
    (:export
        #:hyle  ; Q.Hyle
        #:hyle-id #:hyle-icon #:hyle-name #:hyle-moves #:hyle-sum #:hyle-subsetting #:hyle-expressing
            ;; Q.Hyle.id Q.Hyle.icon ... Q.Hyle.expressing
        #:hyle-definition
        ))

;; model of data items on a wikibase instance - typically but not necessarily Wikidata. ex.: data item (Q80423)
(defpackage :vxsh-suite.wavebuild.wikibase
    (:export
        #:q   ; represents "Q..." item namespace
        #:p   ; represents "P..." property namespace
        #:l)) ; represents "L..." lexeme namespace. really just here for completeness

(defpackage :vxsh-suite.wavebuild.internal  ; system elements expected by a typical dictionary - see system.dict
    ;; import HYLE fields considering they are internal mechanics
    ;;(:shadowing-import-from :vxsh-suite.wavebuild.hyle-struct
        ;;#:hyle  #:hyle-id #:hyle-icon #:hyle-name #:hyle-moves #:hyle-sum #:hyle-subsetting #:hyle-expressing)
        ;; wikibase namespaces are not considered core Wavebuilder mechanics.
    (:export
        #| hyle                Q...  |#   #:mechanic            ; Q.Hyle.Mechanic
        #| mechanic            Q...  |#   #:internal-mechanic   ; Q.Hyle.Internal-Mechanic
        #| internal-mechanic   Q...  |#   #:characteristic      ; Q.Hyle.Characteristic
        #| internal-mechanic   Q...  |#   #:synonym-of          ; Q.Hyle.SynonymOf
        #| hyle                hyle  |#   #:combination         ; Q.Hyle.Combination
        #| combination         Q...  |#   #:unary-operator      ; Q.Hyle.Unary-Operator
        #| combination         Q...  |#   #:binary-operator     ; Q.Hyle.Binary-Operator
        #| unary-operator      Q...  |#   #:default-result      ; Q.Hyle.Default-Result
        #| hyle                Q...  |#   #:entity              ; Q.Hyle.Entity
        #| unary-operator      Q...  |#   #:entity-abilities    ; Q.Hyle.Entity-Abilities
        #| entity-abilities    Q...  |#   #:entity-techniques   ; Q.Hyle.Entity-Techniques
        #| internal-mechanic   Q...  |#   #:dictionary))        ; Q.Hyle.Dictionary

(defpackage :vxsh-suite.wavebuild.l     ; package for lexeme-style elements
    ;; a lexeme contains "a set of words that are related through inflection",  [*L]  and in the case of Wikibase, multiple senses of the same inflected term
    (:shadowing-import-from :vxsh-suite.wavebuild.wikibase #:l)
    (:export #:l))  ; represents "L. ..." in element IDs / used to mediate package dependency

(defpackage :vxsh-suite.wavebuild.q     ; package for most currently-loaded elements
    (:export
        #:q  ; used to mediate package dependency
        ;; system elements expected in a typical dictionary
        #:hyle
        #:hyle-id #:hyle-icon #:hyle-name #:hyle-moves #:hyle-sum #:hyle-subsetting #:hyle-expressing #:hyle-definition
        #:hyle-system #:hyle-systemoperator #:hyle-characterizes #:hyle-synonymof
        #:hyle-combination
        #:hyle-dictionary
        #:hyle-default #:hyle-abilities #:hyle-techniques
        #:hyle-dictionary  #:dict-system  ; system dictionary
        ))


;;; specific lexeme namespaces
;; this current list of packages should not be considered exhaustive

(defpackage :vxsh-suite.wavebuild.l.de    ; lexeme package for GERMAN terms
    (:shadowing-import-from :vxsh-suite.wavebuild.wikibase #:l))
(defpackage :vxsh-suite.wavebuild.l.en    ; lexeme package for ENGLISH terms
    (:shadowing-import-from :vxsh-suite.wavebuild.wikibase #:l))
(defpackage :vxsh-suite.wavebuild.l.la    ; lexeme package for LATIN terms
    (:shadowing-import-from :vxsh-suite.wavebuild.wikibase #:l))

;;=> en.wikipedia.org/wiki/Lexeme  *L
