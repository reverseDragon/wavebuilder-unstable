;;;; wavebuild-dict.lisp - parse dictionary files (.dict)
(defpackage :vxsh-suite.wavebuild.dict
    (:shadowing-import-from :vxsh-suite.wavebuild.hyle
        #:element-name-to-symbol
        #:make-hyle #:hyle-p  #:hyle-name #:hyle-subsetting
        ;; these functions are being referenced semantically as well as for their function code
        #:hyle-id #:hyle-icon #:hyle-sum #:hyle-moves)
    (:shadowing-import-from :vxsh-suite.wavebuild.combine
        #:hyle-string #:hyle-list
        #:set-hyle
        #:hyle-mix #:hyle-mix*)
    (:use :common-lisp)
    (:export
        #:*line-terminators*  ; #::ensure-line-terminators
        ;; #::parse-rule #::parser-at-id #::parser-colon-score #::parser-equal-score #::parser-bracket-icon #::parser-comment-line #::parser-empty-line #::parser-definition #::parser-element-id #::parser-null
        #:line-to-combination #:line-to-list
        #:load-dictionary #:load-dictionary*
        ))
(in-package :vxsh-suite.wavebuild.dict)

;; example dictionary line:
;; Q.Hyle.Connector            Q1147639       @Q.Hyle.Dictionary      dictionary          = 0  [📗]

(defvar *line-terminators*)           ; sequence of characters that separate line values
(defun ensure-line-terminators ()     ;   should be a string, with a default of " "
    (if (boundp '*line-terminators*)
        *line-terminators*  " "))

(defparameter *item-strings*  (vector "Q.HYLE." "Q.Hyle." "Q.Dict."))
(defparameter *item-namespaces*
    (vector :vxsh-suite.wavebuild.hyle :vxsh-suite.wavebuild.internal :vxsh-suite.wavebuild.dict))
(defparameter *lexeme-strings*  (vector "L.de." "L.en." "L.la." "L."))
(defparameter *lexeme-namespaces*
    (vector :vxsh-suite.wavebuild.l.de :vxsh-suite.wavebuild.l.en :vxsh-suite.wavebuild.l.la :vxsh-suite.wavebuild.l))


(defun parser-at-id (item)
    (let ((frst (elt item 0)))
    (values
        (if (equal frst #\@)
            (string-left-trim "@ " item)  nil)
        'hyle-id)))

(defun parser-colon-score (item)
    (let (after-colon
          (sum   (make-string-output-stream))
          (moves (make-string-output-stream))
          )
    (loop for chr across item do
        (cond
            ((equal chr #\:)    (setq after-colon t))
            ((null after-colon) (format sum "~a" chr))
            (t                  (format moves "~a" chr))))
    (setq
        sum   (get-output-stream-string sum)
        moves (get-output-stream-string moves))
    (values
        (if (> (length sum) 0)   (parse-integer sum)   nil)
        (if (> (length moves) 0) (parse-integer moves) nil)
        )))

(defun parser-equal-score (item)
    (let ((frst (elt item 0)))
    (if (equal frst #\=)
        (multiple-value-bind (sum moves)
            (parser-colon-score (string-left-trim "= " item))
            (values (list sum moves) 'hyle-sum))
        nil)))

(defun parser-bracket-icon (item)
    (let ((frst (elt item 0)))
    (values
        (if (equal frst #\[)
            (string-right-trim "] " (string-left-trim "[ " item))
            nil)
        'hyle-icon)))

(defun parser-comment-line (item)
    (let ((frst (elt item 0)))
    (values
        (if (equal frst #\#)
            (string-left-trim "# " item)
            nil)
        'parser-comment-line)))

(defun parser-empty-line (item)
    (let* ((terminators (ensure-line-terminators))
           (clean (string-right-trim terminators item)))
    (values
        (if (< (length clean) 1)  clean  nil)
        'parser-empty-line)))

(defun parser-definition (item)  ; parser to extract term definitions, usually from .dict.def files
    (let ((frst (elt item 0)))
    (values
        (if (equal frst #\tab)
            (string-left-trim " " item)  ; #\tab
            nil)
        'parser-comment-line)))

(defun parser-element-id (item)  ; parser to pick up element ID with correct prefixes
    (let ((frst (elt item 0))
          (item-length (length item))
          seven  prefix-number  item-body  namespace)
    (cond
        ((equal item "Q.HYLE")  (setq namespace :vxsh-suite.wavebuild.hyle-struct  item-body item))
        ((and (equal frst #\Q) (>= item-length 7))
            (setq
                seven         (subseq item 0 7)
                prefix-number (position seven *item-strings* :test 'equal)
                namespace
                    (if (null prefix-number)
                        (if (equal (elt item 1) #\.)  :vxsh-suite.wavebuild.q)
                        (elt *item-namespaces* prefix-number))
                item-body
                    (if (null prefix-number) item  (subseq item 7))
                ))
        ((and (equal frst #\L) (>= item-length 5))
            (setq
                seven         (subseq item 0 5)
                prefix-number (position seven *lexeme-strings* :test 'equal)
                namespace
                    (if (null prefix-number)
                        (if (equal (elt item 1) #\.)  :vxsh-suite.wavebuild.l)
                        (elt *lexeme-namespaces* prefix-number))
                item-body
                    (if (null prefix-number)  item  (subseq item 5))
                )))
    (if (null namespace)
        (values nil nil)
        (values
            (element-name-to-symbol item-body :namespace namespace)
            'parser-element-id))))

(defun parser-null (item)
    (values item nil))

(defun parse-rule (fragment rules)  ; apply parse rule to current string
    (let (result result-rule parsed-item)
    (unless (null fragment)
        (loop  for rule  in rules  until (not (null result))  do
            (multiple-value-bind
                (parse-result parse-func) (funcall rule fragment)
                (setq
                    result
                        (if (null parse-result)
                            result  parse-result)
                    result-rule
                        (if (null parse-result)
                            nil  parse-func)
                    ))))
    (values result result-rule)))

(defun line-to-list (line)  ; parse text representation of line into a list
    (let (char-value next-char next-char-b current-string parsed-item result-list
          (total  (- (length line) 1))
          (current-value  (make-string-output-stream))
          (current-string-length 0)
          (terminators  (ensure-line-terminators))
          (parse-rules  (list 'parser-empty-line 'parser-at-id 'parser-equal-score 'parser-bracket-icon 'parser-null))
         )
    (loop for i  from 0 to total  do
        (setq
            char-value   (elt line i)
            next-char    (if (< i total)  (elt line (+ i 1))  #\ )
            next-char-b  (if (< (+ i 2) total)  (elt line (+ i 2))  #\ ))

        (unless (and (find char-value terminators) (find next-char terminators))
            (incf current-string-length)
            (format current-value "~a" char-value)
            )
        (cond
            ((and (find next-char terminators) (find next-char-b terminators))
                (setq
                    current-string
                        (string-left-trim terminators
                            (get-output-stream-string current-value))
                    )
                (multiple-value-bind (parse-result parse-func) (parse-rule current-string parse-rules)
                    (setq parsed-item (cons parse-func parse-result))
                    )
                (when (> current-string-length 0)
                    (setq
                        result-list
                            (cons
                                (if (null parsed-item)
                                    current-string
                                    parsed-item)
                                result-list)))
                (setq
                    ;; reset string collectors
                    current-string-length  0
                    current-value   (make-string-output-stream)
                    ))))
    (reverse result-list)))

(defun line-to-combination (line-text)
    (let* (item item-rule item-value subset-pieces item-id item-name
          (value-list     (line-to-list line-text))
          (total  (- (length value-list) 1))
          (result-element (make-hyle :moves nil))
          )
    ;; BUG: Z
    (loop for i  from 0 to total  do
        (setq
            item  (elt value-list i)
            item-rule  (car item)
            item-value (cdr item)
            subset-pieces
                (if (null item-rule)
                    (cons item-value subset-pieces)
                    subset-pieces
                    ))
        ;; match list values to HYLE fields
        (cond
            ;; assign ID appearing after first two unlabeled values to result element
            ((and (equal item-rule 'hyle-id) (>= (length subset-pieces) 2))
                (setf
                    item-id  (parser-element-id item-value)
                    (hyle-id result-element)  item-id))
            ;; assign result element its :sum, :moves, and :icon
            ((equal item-rule 'hyle-sum)
                (setf
                    (hyle-sum result-element)   (first item-value)
                    (hyle-moves result-element) (second item-value)))
            ((equal item-rule 'hyle-icon)
                (setf (hyle-icon result-element)  item-value))
            ;; if found, assign third unlabeled value to result element
            ((and (null item-rule) (> (length subset-pieces) 2))
                (setf
                    (hyle-name result-element)  item-value)
                (when (null (hyle-id result-element))
                    (setf
                        (hyle-id result-element)
                            (or
                                (parser-element-id item-value)
                                (hyle-id (vxsh-suite.wavebuild.combine:get-hyle item-value))
                                (element-name-to-symbol item-value)
                                ))))
            ;; take the first two unlabeled values and assume result is the combination :subsetting them
            ((and (null item-rule) (<= (length subset-pieces) 2))
                (setf
                    (hyle-subsetting result-element)
                    (cons
                        ;; attempt to retrieve proper IDs based on synonyms
                        (or
                            (hyle-id (vxsh-suite.wavebuild.combine:get-hyle item-value))
                            item-value)
                        (hyle-subsetting result-element)
                        )))))
    ;;(when (equal (hyle-id result-element) 'VXSH-SUITE.WAVEBUILD.Q::DRAGON-BALL-Z)  (print (list "RESULT" result-element)))
    ;;(print (list "RESULT" (hyle-name result-element) (hyle-id result-element) (hyle-subsetting result-element)))
    ;; return line information as HYLE structure
    result-element))

(defun load-dictionary (dictionary-path  &key text)
    (let (result-line previous-line subset hyle-a hyle-b
          (dictionary-text  ; read dictionary into a list of strings
              (cond
                  ((pathnamep dictionary-path)
                  (uiop:read-file-lines dictionary-path))
                  (t nil)))
          (parse-rules (list 'parser-empty-line 'parser-comment-line))
          )
    (print (list "DICTIONARY PATH" dictionary-path))
    ;; loop through lines of dictionary parsing each one
    (dolist (line dictionary-text)
        (multiple-value-bind (parse-result parse-func) (parse-rule line parse-rules)
        (cond
            ((or (equal parse-func 'parser-comment-line) (equal parse-func 'parser-empty-line))
                '())
            ;;((equal parse-func 'parser-definition)
                ;;(setq result-line line)
                ;;(set-hyle)
                ;;)
            (t
                (setq
                    result-line (line-to-combination line)
                    subset      (hyle-subsetting result-line)
                    hyle-a      (first subset)
                    hyle-b      (second subset))
                ;; add combination to model
                (when (>= (length subset) 2)
                    (hyle-mix* hyle-a hyle-b :result result-line))
                )))
        (setq previous-line line))
    (hyle-list :text text)))

(defun load-dictionary* (&rest dictionaries)
    (dolist (dict dictionaries)  (load-dictionary dict)))
