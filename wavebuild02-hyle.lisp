;;;; wavebuild-hyle.lisp - combinable element structure
;; the HYLE structure is defined purely with Lisp's lower-level functions instead of using DEFSTRUCT or CLOS,
;; for the purposes of having simple and self-explanatory code that is easier to maintain or build on
(defpackage :vxsh-suite.wavebuild.hyle
	(:shadowing-import-from :vxsh-suite.wavebuild.q  ; <- this package is used as a namespace
		;; this symbol does nothing so far and is imported as a reminder to have ".q" package accessible
		#:q)
	(:use :common-lisp)
	(:export
		;; #::*package-names* #::*package-prefixes*
		#:element-name-to-symbol #:element-string-to-symbol  ; #::element-symbol-short-package
		#:hyle  ; used only to mark structures
		#:hyle-p #:make-hyle  #:hyle-fallback-info
		#:hyle-id #:hyle-icon #:hyle-name #:hyle-width #:hyle-moves #:hyle-sum #:hyle-subsetting #:hyle-expressing
		#:hyle-sort-score  ; #::get-hyle-sum
		#:hyle< #:hyle-moves-+ #:hyle-sum-+ #:hyle-subset-+
		#:hyle-query
		))
(in-package :vxsh-suite.wavebuild.hyle)

(defparameter *package-names*
    (vector "VXSH-SUITE.WAVEBUILD.INTERNAL" "VXSH-SUITE.WAVEBUILD.HYLE-STRUCT" "VXSH-SUITE.WAVEBUILD.DICT"
            "VXSH-SUITE.WAVEBUILD.WIKIBASE" "VXSH-SUITE.WAVEBUILD.L" "VXSH-SUITE.WAVEBUILD.Q"
            "VXSH-SUITE.WAVEBUILD.L.DE" "VXSH-SUITE.WAVEBUILD.L.EN" "VXSH-SUITE.WAVEBUILD.L.LA"))
(defparameter *package-prefixes*
    (vector "SYS" "HYLE" "DICT" "W" "L" "Q"  "L-de" "L-en" "L-la"))


(defun element-name-to-symbol (name  &key namespace)  ; normalize element name to symbol in Q package
	(let (normalized body result)
	(cond
	((symbolp name) name)
	((stringp name)
		(setq
			normalized
				(substitute-if #\-
					(lambda (str)  (find str (list #\  #\.)))
					(string-downcase name))
			;; remove Q- unless the element is a Wikidata item
			;; I first used 'string-left-trim until it butchered 'QUEEN into 'UEEN. oops
			body 
				(if (and  (equal (elt normalized 0) #\q)  (equal (elt normalized 1) #\-))
					(subseq normalized 2)  normalized)
			result
				(if (null (parse-integer body :junk-allowed t))
					body  normalized))
		(intern
			(string-upcase (format nil "~a" result))
			(if (null namespace)  :vxsh-suite.wavebuild.q  namespace)
			)))))
;; deprecated name
(defun element-string-to-symbol (name)  (element-name-to-symbol name))

(defun element-symbol-short-package (name)
    (let* ((element-package (package-name (symbol-package name)))
          (short-prefix-number (position element-package *package-names* :test 'equal)))
    (if (null short-prefix-number)
        nil
        (elt *package-prefixes* short-prefix-number)
        )))

(defun hyle-p (element)
	(and
		(vectorp element)
		(equal (elt element 0) 'hyle)))

(defun hyle-id (id  &key text)
    (let* ((id-symbol
             (cond  ; pull :id value out of HYLE structure
                 ((null id)    nil)
                 ((hyle-p id)  (elt id 1))
                 (t id)))
           (canonical-id
             (if (null id-symbol)                     ; if :id is a symbol or string, and not NIL etc
                 id-symbol                            ; normalize all IDs to same package
                 (element-name-to-symbol id-symbol))) ; otherwise just return it
           namespace)
    (cond
        ((null text)          canonical-id)
        ((null canonical-id)  "(NONE)")
        ((symbolp canonical-id)
            (setq namespace (element-symbol-short-package canonical-id))
            (format nil "~a~a~a"
                (if (null namespace)  ""  namespace)
                (if (null namespace)  ""  #\:)
                canonical-id))
        (t  (format nil "~a" canonical-id))
        )))

(defun hyle-icon (element)
    (cond
        ((hyle-p element)  (elt element 2))
        (t "🧩")))

(defun hyle-name (element)
    (cond
        ((hyle-p element)  (elt element 3))
        ((stringp element) element)
        (t element)))

(defun hyle-width (element)
	(let ((name (hyle-name element)))
	(if (stringp name)
		(length name)  0)))

(defun get-hyle-sum (element field &key force-integer)  ; helper to get sum and moves
    (let (result)
    (setq result
        (cond
            ((hyle-p element)   (elt element field))
            ;; integer: if sum was already fetched out of HYLE
            ((integerp element) element)
            ;; string: mainly to be used for 'put
            ((stringp element)  1)
            ;; for an unrecognized datatype, return NIL
            (t nil)))
    ;; distinguish between NIL and zero sums in most cases, or else :force-integer
    (if (null result)
        (if (null force-integer)  nil  0)
        result)))

(defun hyle-moves (element &key force-integer)
	(get-hyle-sum element 4 :force-integer force-integer))

(defun hyle-sum  (element &key force-integer)
	(get-hyle-sum element 5 :force-integer force-integer))

(defun hyle-subsetting (element)  (elt element 6))
(defun hyle-expressing (element)  (elt element 7))

(defun (setf hyle-id)         (value element)  (setf (aref element 1) value))
(defun (setf hyle-icon)       (value element)  (setf (aref element 2) value))
(defun (setf hyle-name)       (value element)  (setf (aref element 3) value))
(defun (setf hyle-moves)      (value element)  (setf (aref element 4) value))
(defun (setf hyle-sum)        (value element)  (setf (aref element 5) value))
(defun (setf hyle-subsetting) (value element)  (setf (aref element 6) value))
(defun (setf hyle-expressing) (value element)  (setf (aref element 7) value))


(defun hyle-fallback-info (hyle field-func field-override known-hyle)
	(let (result)
	(setf
		result  field-override
		result
			(if (and (null result) (not (null hyle)))
				(funcall field-func hyle)  result)
		result
			(if (and (null result) (not (null known-hyle)))
				(funcall field-func known-hyle)  result
				))))

(defun make-hyle
	(&key id icon name descriptor existing (moves nil movesp) sum subsetting expressing)
	(cond
		;; if passed generic descriptor, apply it as either the name or ID
		((stringp descriptor)
			(setq name descriptor))
		((symbolp descriptor)
			(setq
				id   descriptor
				name
					(if (null descriptor)
						nil
						(symbol-name descriptor)
						)))
		((hyle-p descriptor)
			(setf
				id          (hyle-fallback-info existing   'hyle-id         id    descriptor)
				icon        (hyle-fallback-info descriptor 'hyle-icon       icon  existing)
				name        (hyle-fallback-info descriptor 'hyle-name       name  existing)
				moves       (hyle-fallback-info descriptor 'hyle-moves      moves existing)
				sum         (hyle-fallback-info descriptor 'hyle-sum        sum   existing)
				subsetting  (hyle-fallback-info descriptor 'hyle-subsetting subsetting  existing)
				expressing  (hyle-fallback-info descriptor 'hyle-expressing expressing  existing)
				)))
	;; zero out moves and sum if they are empty in any way
	(setf
		moves
			(if (null moves)
				(if (null movesp)  nil  0)
				moves)
		sum  (if (null sum)  0  sum)
		)
	;; if no :id given, attempt to create one from element name
	(if (null id)  (setq id name))
	;; assuming :id has been created, normalize :id
	(when (or (symbolp id) (stringp id))
		(setq id  (element-name-to-symbol id)))
	;; by default an element is always a subset of itself,
	;;   and is :expressing all the elements it subsets
	(when (and (null subsetting) (not (null id)))
		(setq subsetting (list id)))
	(when (null expressing)  (setq expressing subsetting))
	;; return HYLE structure
	(vector 'hyle id icon name moves sum subsetting expressing))


;;; HYLE "mathematics"

(defun hyle-sort-score (hyle-a hyle-b)
;; this is separate mostly for debug purposes
    (let ((sum-weight 4) (moves-weight 4) (name-weight 1))
    
    (if
    (or
        (>
            (hyle-moves hyle-a :force-integer t)
            (hyle-moves hyle-b :force-integer t))
        (>
            (hyle-sum hyle-a :force-integer t)
            (hyle-sum hyle-b :force-integer t))
        (string>=
            (symbol-name (hyle-id hyle-a))
            (symbol-name (hyle-id hyle-b)))
        )
    1 0)
    
    #|(+
        (if (>
                (hyle-sum hyle-a :force-integer t)
                (hyle-sum hyle-b :force-integer t))
            sum-weight   0)
        (if (>
                (hyle-moves hyle-a :force-integer t)
                (hyle-moves hyle-b :force-integer t))
            moves-weight 0)
        (if (string>=
                (symbol-name (hyle-id hyle-a))
                (symbol-name (hyle-id hyle-b)))
            name-weight  0)
        )|#
))

(defun hyle< (a b)  ; return sorting order of two HYLE structures
   ;; convert both a & b to hyle
    (let ((hyle-a (make-hyle :descriptor a))
          (hyle-b (make-hyle :descriptor b)))
    (<
        (hyle-sort-score hyle-a hyle-b)
        (hyle-sort-score hyle-b hyle-a))))

(defun hyle-moves-+ (a b) ; add up hyle moves
	;; adding up numbers of moves follows the pattern of set theory -
	;; it is as if the plus sign in the equation counts as 1, but otherwise 0 = ∅ = 0
	;; I first used:  (+ a b 1)
	;; but this doesn't get the real number of moves for things like combining the same element, which should only add 1 move despite being weirdly inefficient
	(+ 1  (max (hyle-moves a) (hyle-moves b))))

(defun hyle-sum-+ (a b)  ; add up hyle sums to produce result sum
	;; in general, a :sum cannot be less than 1
	;; some exceptions are granted for the most basic system elements that precede "concept"/HYLE
	(max
		;; if the elements are the same, treat one of them as a starting 1-move element
		(if (equal (hyle-id a) (hyle-id b))
			(+ (hyle-sum a) 1)
			(+ (hyle-sum a) (hyle-sum b)))
		1))

(defun hyle-subset-+ (&rest subsets)  ; add up total set of ancestors
    ;; at first I included 'remove-duplicates until I realized it would be better to leave them for icon inferences
    (let ((result nil))
    (dolist (subsetting subsets)
        (setq
            subsetting
                ;; if passed :id instead of list, append it properly
                (if (or (hyle-p subsetting) (symbolp subsetting))
                    (list subsetting)
                    ;; remove NIL ancestors
                    (mapcan  #'(lambda (x) (and (not (null x)) (list x)))
                        subsetting))
            result
                (if (null result)
                    subsetting  (append subsetting result))
            ))
    result))

(defun hyle-query (a b)  ; query used to look up / store element combinations
	(let ((sorted
		(stable-sort (list a b) #'hyle<)))
	(values
		(cons  (hyle-id (first sorted))  (hyle-id (second sorted)))
		sorted)))

;; LATER: when there are inferred combinations, create a function to guess icon based on counting repeated emoji inside element's nearest ancestors
;; icon-strings should be interned as system elements with an :id of their unicode number(s) and a :name of their characters, just to make it really easy to test they are the same
